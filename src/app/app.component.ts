import { Component } from '@angular/core';
import { User } from './model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';
  users: User[] = [];
  onNewUser(user: User): void {
    console.log("New user: ", user);
    this.users.push(user);
  }
}
