import { Component, OnInit, ViewChild, Output, EventEmitter } from '@angular/core';
import { NgForm } from '@angular/forms';
import { User } from '../model';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  @ViewChild("regForm") regForm: NgForm;
  @Output('newUser') newUser: EventEmitter<User> = new EventEmitter<User>();

  constructor() { }

  ngOnInit() {
  }

  processForm():void {
    console.log("Processing web form...");
    let user:User = {
      name: this.regForm.value.name,
      email: this.regForm.value.email
    };
   
    console.info("user: ", user);
    this.newUser.emit(user);

    this.regForm.resetForm();
  }

}
