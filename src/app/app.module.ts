import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';


import { AppComponent } from './app.component';
import { RegisterComponent } from './components/register.component';
import { UserListComponent } from './components/user-list.component';


@NgModule({
  declarations: [
    AppComponent,
    RegisterComponent,
    UserListComponent
  ],
  imports: [
    BrowserModule,
    FormsModule // make it available to rest of module
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
